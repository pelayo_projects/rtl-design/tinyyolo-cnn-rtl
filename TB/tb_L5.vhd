
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
USE IEEE.MATH_REAL.ALL;

LIBRARY work;
USE work.YOLO_pkg.ALL;

LIBRARY STD;
USE STD.TEXTIO.ALL;

ENTITY tb_L5 IS
END tb_L5;

ARCHITECTURE behavior OF tb_L5 IS

   SIGNAL reset : STD_LOGIC;
   SIGNAL clk : STD_LOGIC;

   CONSTANT clk_period : TIME := 10 ns;

   COMPONENT layer5 IS
      PORT (
         reset : IN STD_LOGIC;
         clk : IN STD_LOGIC;
         validIN : IN STD_LOGIC;
         start : IN STD_LOGIC;
         -- Entradas de YOLO.
         dataIN: IN std_logic_vector(53 downto 0) ;
         -- Salidas de YOLO.  
         DataOut : OUT STD_LOGIC_VECTOR(53 DOWNTO 0);
         -- mpdataout : OUT STD_LOGIC_VECTOR(5 DOWNTO 0);
         --mpvalidOut : OUT STD_LOGIC;
         --cvdataout : OUT STD_LOGIC_VECTOR(5 DOWNTO 0);
         --cvvalidOut : OUT STD_LOGIC;
         validOut : OUT STD_LOGIC);
   END COMPONENT;
   -- Archivos de texto

   FILE entrada : TEXT IS IN "C:\Users\asdoblado\Google Drive\MUISE\TFM\results\testbench\ImageOutL4.txt";
   FILE salida : TEXT IS OUT "C:\Users\asdoblado\Google Drive\MUISE\TFM\results\testbench\ImageOutL5.txt";
   --FILE salida2 : TEXT IS OUT "C:\Users\asdoblado\Google Drive\MUISE\TFM\results\testbench\MPOut.txt";
   --FILE salida3 : TEXT IS OUT "C:\Users\asdoblado\Google Drive\MUISE\TFM\results\testbench\CVOut.txt";
   -- Senales de control

   CONSTANT latencia : INTEGER := delaymem(1);

   SIGNAL start : STD_LOGIC;
   SIGNAL ValidIn : STD_LOGIC;


   SIGNAL DataIn : STD_LOGIC_VECTOR(53 DOWNTO 0);
   SIGNAL DataOut : STD_LOGIC_VECTOR(53 DOWNTO 0);
   SIGNAL validOut : STD_LOGIC;

   -- SIGNAL mpDataOut : STD_LOGIC_VECTOR(5 DOWNTO 0);
   -- SIGNAL mpvalidOut : STD_LOGIC;

   --SIGNAL cvDataOut : STD_LOGIC_VECTOR(5 DOWNTO 0);
   -- SIGNAL cvvalidOut : STD_LOGIC;

   SIGNAL simular : STD_LOGIC;

BEGIN

   YOLO : layer5
   PORT MAP(
      reset => reset,
      start => start,
      clk => clk,
      validIN => validIN,
      -- Entradas de YOLO. 
      datain => DataIn,
      -- Salidas de YOLO.  
      DataOut => DataOut,
      --mpdataout => mpdataout,
      --mpvalidOut => mpvalidOut,
      --cvdataout => cvdataout,
      --cvvalidOut => cvvalidOut,
      validOut => validOut);

   reset <= '1', '0' AFTER 10 ns, '1' AFTER 25 ns;

   -- Clock process definitions
   clk_process : PROCESS
   BEGIN
      clk <= '0';
      WAIT FOR clk_period/2;
      clk <= '1';
      WAIT FOR clk_period/2;
   END PROCESS;

   lectura : PROCESS

      VARIABLE linea_in : line;
      VARIABLE dato : INTEGER;

   BEGIN

      simular <= '1';

      WAIT UNTIL reset = '0';
      WAIT UNTIL reset = '1';

      start <= '1';
      validIn <= '1';

      WHILE NOT endfile(entrada) LOOP

         FOR i IN 0 TO 8 LOOP
            readline(entrada, linea_in);
            read(linea_in, dato);
            DataIn((i*6)+5 downto (i*6)) <= STD_LOGIC_VECTOR(to_signed(dato, 6));
         END LOOP;
         WAIT FOR clk_period;
      END LOOP;

         DataIn <= (OTHERS => '0');
      validIn <= '0';
      -- WAIT FOR 2*latencia * clk_period;
      --  simular <= '0'; -- dejamos de escribir en el archivo de salida
      --WAIT FOR 4 * clk_period;
      -- readline(entrada, linea_in); -- generamos un error para que pare el simulador

      WAIT;
   END PROCESS;

   escrituraout : PROCESS
      VARIABLE dato_salida : INTEGER;
      VARIABLE linea_out : line;
   BEGIN
      WAIT FOR 10 ns; -- latencia del circuito	 ---- ADJUST ----	
      WHILE simular = '1' LOOP
         IF validout = '1' THEN
            FOR i IN 0 TO 8 LOOP
               dato_salida := to_integer(signed(dataout((i*6)+5 DOWNTO (i*6))));
               write(linea_out, dato_salida);
               writeline(salida, linea_out);
            END LOOP;
         END IF;
         WAIT FOR clk_period; -- Esperamos un ciclo de reloj
      END LOOP; -- Volvemos al principio del bucle
      WAIT;
   END PROCESS;

   --   escritura : PROCESS

   --      VARIABLE dato_salida0 : INTEGER;
   --      VARIABLE dato_salida1 : INTEGER;
   --      VARIABLE dato_salida2 : INTEGER;
   --      VARIABLE dato_salida3 : INTEGER;
   --      VARIABLE dato_salida4 : INTEGER;
   --      VARIABLE dato_salida5 : INTEGER;
   --      VARIABLE dato_salida6 : INTEGER;
   --      VARIABLE dato_salida7 : INTEGER;
   --      VARIABLE dato_salida8 : INTEGER;

   --      VARIABLE linea_out : line;
   --   BEGIN
   --      WAIT FOR 10000 ns; -- latencia del circuito	 ---- ADJUST ----	
   --      WHILE simular = '1' LOOP
   --         IF validOut = '1' THEN

   --            dato_salida0 := to_integer(signed(DataOut(5 downto 0)));
   --            write(linea_out, dato_salida0);
   --            writeline(salida, linea_out);

   --            dato_salida1 := to_integer(signed(DataOut(11 downto 6)));
   --            write(linea_out, dato_salida1);
   --            writeline(salida, linea_out);

   --            dato_salida2 := to_integer(signed(DataOut(17 downto 12)));
   --            write(linea_out, dato_salida2);
   --            writeline(salida, linea_out);

   --            dato_salida3 := to_integer(signed(DataOut(23 downto 18)));
   --            write(linea_out, dato_salida3);
   --            writeline(salida, linea_out);

   --            dato_salida4 := to_integer(signed(DataOut(29 downto 24)));
   --            write(linea_out, dato_salida4);
   --            writeline(salida, linea_out);

   --            dato_salida5 := to_integer(signed(DataOut(35 downto 30)));
   --            write(linea_out, dato_salida5);
   --            writeline(salida, linea_out);

   --            dato_salida6 := to_integer(signed(DataOut(41 downto 36)));
   --            write(linea_out, dato_salida6);
   --            writeline(salida, linea_out);

   --            dato_salida7 := to_integer(signed(DataOut(47 downto 42)));
   --            write(linea_out, dato_salida7);
   --            writeline(salida, linea_out);

   --            dato_salida8 := to_integer(signed(DataOut(53 downto 48)));
   --            write(linea_out, dato_salida8);
   --            writeline(salida, linea_out);

   --         END IF;
   --         WAIT FOR clk_period; -- Esperamos un ciclo de reloj
   --      END LOOP; -- Volvemos al principio del bucle
   --      WAIT;
   --   END PROCESS;
   --
   --   escritura2 : PROCESS
   --      VARIABLE dato_salida2 : INTEGER;
   --      VARIABLE linea_out2 : line;
   --   BEGIN
   --      WAIT FOR 10 ns; -- latencia del circuito	 ---- ADJUST ----	
   --      WHILE simular = '1' LOOP
   --         IF mpvalidOut = '1' THEN
   --            dato_salida2 := to_integer(signed(mpdataout));
   --            write(linea_out2, dato_salida2);
   --            writeline(salida2, linea_out2);
   --         END IF;
   --         WAIT FOR clk_period; -- Esperamos un ciclo de reloj
   --      END LOOP; -- Volvemos al principio del bucle
   --      WAIT;
   --   END PROCESS;

   --   escritura3 : PROCESS
   --      VARIABLE dato_salida3 : INTEGER;
   --      VARIABLE linea_out3 : line;
   --   BEGIN
   --      WAIT FOR 10 ns; -- latencia del circuito	 ---- ADJUST ----	
   --      WHILE simular = '1' LOOP
   --         IF validout = '1' THEN
   --            dato_salida3 := to_integer(signed(dataout(15 downto 0)));
   --            write(linea_out3, dato_salida3);
   --            writeline(salida3, linea_out3);
   --         END IF;
   --         WAIT FOR clk_period; -- Esperamos un ciclo de reloj
   --      END LOOP; -- Volvemos al principio del bucle
   --      WAIT;
   --   END PROCESS;
END;