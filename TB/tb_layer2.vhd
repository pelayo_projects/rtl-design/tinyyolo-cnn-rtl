
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
USE IEEE.MATH_REAL.ALL;

LIBRARY work;
USE work.YOLO_pkg.ALL;

LIBRARY STD;
USE STD.TEXTIO.ALL;

ENTITY tb_layer2 IS
END tb_layer2;

ARCHITECTURE behavior OF tb_layer2 IS

    CONSTANT clk_period : TIME := 10 ns;

    COMPONENT layer2 IS
        GENERIC (layer : INTEGER := 2);
        PORT (
            reset : IN STD_LOGIC;
            clk : IN STD_LOGIC;
            validIN : IN STD_LOGIC;
            start : IN STD_LOGIC;
            -- Entradas de YOLO. 
            dataIN : IN STD_LOGIC_VECTOR(53 DOWNTO 0);
            -- Salidas de YOLO.  
            DataOut : OUT STD_LOGIC_VECTOR(53 DOWNTO 0);
            validOut : OUT STD_LOGIC);
    END COMPONENT;
    -- Senales de control
    SIGNAL reset : STD_LOGIC;
    SIGNAL clk : STD_LOGIC;
    CONSTANT latencia : INTEGER := delaymem(2);

    SIGNAL start : STD_LOGIC;
    SIGNAL ValidIn : STD_LOGIC;
    SIGNAL datain : STD_LOGIC_VECTOR(53 DOWNTO 0);

    SIGNAL DataOut : STD_LOGIC_VECTOR(53 DOWNTO 0);
    SIGNAL validOut : STD_LOGIC;

BEGIN

    YOLO : layer2
    GENERIC MAP(layer => 2)
    PORT MAP(
        reset => reset,
        start => start,
        clk => clk,
        validIN => validIN,
        -- Entradas de YOLO. 
        DataIn => datain,
        -- Salidas de YOLO.  
        DataOut => DataOut,
        validOut => validOut);

    stimulus : PROCESS
    BEGIN
        -- Put initialisation code here
        reset <= '0';
        DataIn <= (OTHERS => '0');
        validIN <= '0';
        start <= '0';

        WAIT FOR 5 ns;
        reset <= '1';
        start <= '1';
        wait for (latencia*clk_period);
        
        validIN <= '1';
        DataIn <= (OTHERS => '1');

        WAIT FOR latencia* clk_period;

        validIN <= '0';

        WAIT;
    END PROCESS;

    -- Clock process definitions
    clk_process : PROCESS
    BEGIN
        clk <= '0';
        WAIT FOR clk_period/2;
        clk <= '1';
        WAIT FOR clk_period/2;
    END PROCESS;

END;