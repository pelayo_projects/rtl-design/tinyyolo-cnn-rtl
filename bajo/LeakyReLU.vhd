LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

ENTITY LeakyReLU IS
GENERIC(WL: INTEGER:=32);
    PORT (
        datain : IN STD_LOGIC_VECTOR(WL-1 DOWNTO 0);
        dataout : OUT STD_LOGIC_VECTOR(WL-1 DOWNTO 0)
    );
END ENTITY LeakyReLU;

ARCHITECTURE rtl OF LeakyReLU IS

BEGIN

    leaky : PROCESS (datain)
    BEGIN
        CASE datain(WL-1) IS
            WHEN '0' => --si es positivo
                dataout <= datain; --salida tal cual

            WHEN OTHERS => -- si es negativo
                dataout <= datain(WL-1) & datain(WL-1) & datain(WL-1) & datain(WL-1 DOWNTO 3); --se multiplica x0.125
        END CASE;
    END PROCESS leaky;
END ARCHITECTURE rtl;