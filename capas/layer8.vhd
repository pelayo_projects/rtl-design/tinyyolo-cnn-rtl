LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;

LIBRARY work;
USE work.YOLO_pkg.ALL;

--Aquí se comprueba el funcionamiento de una sóla capa

ENTITY layer8 IS
    PORT (
        clk : IN STD_LOGIC;
        reset : IN STD_LOGIC;
        start : IN STD_LOGIC;

        dataIN : IN STD_LOGIC_VECTOR(53 DOWNTO 0);
        validIN : STD_LOGIC;

        dataOUT : OUT STD_LOGIC_VECTOR(5 DOWNTO 0);
        validOut : OUT STD_LOGIC
    );
END layer8;

ARCHITECTURE rtl OF layer8 IS

    CONSTANT rst_val : STD_LOGIC := '0';
    CONSTANT layer : INTEGER := 8;

    COMPONENT L8_BNROM
        PORT (            clk : IN STD_LOGIC;
            coefs : OUT STD_LOGIC_VECTOR((32 * kernels(layer)) - 1 DOWNTO 0);
            address : IN unsigned(BNbitsAddress(layer) DOWNTO 0));
    END COMPONENT;

    COMPONENT L8_WROM
        PORT (            clk : IN STD_LOGIC;
            weight : OUT STD_LOGIC_VECTOR((9 * kernels(layer)) - 1 DOWNTO 0);
            address : IN unsigned(weightsbitsAddress(layer) - 1 DOWNTO 0));
    END COMPONENT;

    COMPONENT ConvControl
        GENERIC (
            layer : INTEGER := 8
        );
        PORT (
            clk : IN STD_LOGIC;
            reset : IN STD_LOGIC;

            validIn : IN STD_LOGIC;

            startLBuffer : OUT STD_LOGIC;
            enableLBuffer : OUT STD_LOGIC;

            addressweight : OUT unsigned(weightsbitsAddress(LAYER) - 1 DOWNTO 0);
            addressbn : OUT unsigned(bits(filters(layer)/kernels(layer) - 1) - 1 DOWNTO 0);

            validOut : OUT STD_LOGIC
        );
    END COMPONENT;

    COMPONENT ConvDP
        GENERIC (layer : INTEGER := 8);
        PORT (
            clk : IN STD_LOGIC;
            reset : IN STD_LOGIC;

            datain : IN STD_LOGIC_VECTOR((9 * layerbits(layer)) - 1 DOWNTO 0);
            Weights : IN STD_LOGIC_VECTOR(8 DOWNTO 0);
            Ynorm : IN signed(15 DOWNTO 0);
            Bnorm : IN signed(15 DOWNTO 0);

            startLbuffer : IN STD_LOGIC;
            enableLbuffer : IN STD_LOGIC;

            dataout : OUT STD_LOGIC_VECTOR(5 DOWNTO 0));
    END COMPONENT;

    COMPONENT MemControlLL
        GENERIC (
            layer : INTEGER := 8);
        PORT (
            clk : IN STD_LOGIC;
            reset : IN STD_LOGIC;
            start : IN STD_LOGIC;

            we : IN STD_LOGIC;
            weRAM : OUT STD_LOGIC;
            wMemOdd : OUT STD_LOGIC;
            wBank : OUT unsigned(3 DOWNTO 0);
            waddress : OUT unsigned(bitsAddress(layer) - 1 DOWNTO 0);

            rMem : OUT unsigned(bits(kernels(layer)) - 1 DOWNTO 0);
            rMemOdd : OUT STD_LOGIC;
            rBank : OUT unsigned(3 DOWNTO 0);
            raddress : OUT unsigned(bitsAddress(layer) - 1 DOWNTO 0);

            validOut : OUT STD_LOGIC);
    END COMPONENT;

    COMPONENT MemDPLL
        GENERIC (layer : INTEGER := 8);
        PORT (
            clk : IN STD_LOGIC;
            reset : IN STD_LOGIC;

            rMem : IN unsigned(bits(kernels(layer)) - 1 DOWNTO 0);
            rMemOdd : IN STD_LOGIC;
            raddress : IN unsigned(bitsAddress(layer) - 1 DOWNTO 0);
            rbank : IN unsigned(3 DOWNTO 0);
            Dout : OUT STD_LOGIC_VECTOR(5 DOWNTO 0);

            Din : IN STD_LOGIC_VECTOR((kernels(layer) * 6) - 1 DOWNTO 0);
            we : IN STD_LOGIC;
            wMemOdd : IN STD_LOGIC;
            wBank : IN unsigned(3 DOWNTO 0);
            waddress : IN unsigned(bitsAddress(layer) - 1 DOWNTO 0));
    END COMPONENT;

    SIGNAL startLBuffer : STD_LOGIC;
    SIGNAL enableLBuffer : STD_LOGIC;
    SIGNAL addressweight : unsigned(weightsbitsAddress(layer) - 1 DOWNTO 0);
    SIGNAL addressbn : unsigned(bits(filters(layer)/kernels(layer) - 1) - 1 DOWNTO 0);
    SIGNAL validoutCV : STD_LOGIC;

    SIGNAL ROMweight : STD_LOGIC_VECTOR((9 * kernels(layer)) - 1 DOWNTO 0);
    SIGNAL invROMweight : STD_LOGIC_VECTOR((9 * kernels(layer)) - 1 DOWNTO 0);

    SIGNAL ROMcoefs : STD_LOGIC_VECTOR((32 * kernels(layer)) - 1 DOWNTO 0);

    SIGNAL outCVDP : STD_LOGIC_VECTOR((6 * kernels(layer)) - 1 DOWNTO 0);

    SIGNAL we : STD_LOGIC;
    SIGNAL wMemOdd : STD_LOGIC;
    SIGNAL wBank : unsigned(3 DOWNTO 0);
    SIGNAL waddress : unsigned(bitsAddress(layer) - 1 DOWNTO 0);

    SIGNAL rMem : unsigned(bits(kernels(layer)) - 1 DOWNTO 0);
    SIGNAL rMemOdd : STD_LOGIC;
    SIGNAL rbank : UNSIGNED(3 DOWNTO 0);
    SIGNAL raddress : unsigned(bitsAddress(layer) - 1 DOWNTO 0);

BEGIN

    --CONTROL

    ConvUC : ConvControl
    GENERIC MAP(Layer => Layer)
    PORT MAP(
        clk => clk, reset => reset,
        validIn => validIN,
        startLBuffer => startLBuffer, enableLBuffer => enableLBuffer,
        addressweight => addressweight,
        addressbn => addressbn,
        validOut => validoutCV);

    BNROM : L8_BNROM
    PORT MAP(clk => clk,
        coefs => ROMcoefs,
        address => addressbn
    );

    WROM : L8_WROM
    PORT MAP(clk => clk,
        weight => ROMweight,
        address => addressweight);

    invweight : PROCESS (ROMweight)
    BEGIN
        FOR i IN 0 TO 8 LOOP
            FOR j IN 0 TO kernels(layer) - 1 LOOP
                invROMweight((j * 9) + i) <= ROMweight(((j + 1) * 9) - 1 - i);
            END LOOP;
        END LOOP;
    END PROCESS invweight;

    kernelsDP : FOR I IN 0 TO kernels(layer) - 1 GENERATE
        ConvUP : ConvDP
        GENERIC MAP(Layer => Layer)
        PORT MAP(
            clk => clk, reset => reset,
            DataIn => datain,
            weights => invROMweight((I * 9) + 8 DOWNTO I * 9),
            Ynorm => signed(ROMcoefs((I * 32) + 15 DOWNTO I * 32)),
            Bnorm => signed(ROMcoefs((I * 32) + 31 DOWNTO (I * 32) + 16)),
            startLBuffer => startLBuffer,
            enableLBuffer => enableLBuffer,
            dataout => outCVDP((I * 6) + 5 DOWNTO I * 6)
        );
    END GENERATE kernelsDP;

    -- MEMORIA

    memUC : MemControlLL
    GENERIC MAP(LAYER => LAYER)
    PORT MAP(
        clk => clk, reset => reset,
        start => start, 
        
        we => validoutCV,
        weram => we,
        wmemodd => wmemodd,
        wBank => wBank,
        waddress => waddress,

        rmem => rmem,
        rmemodd => rmemodd,
        rbank => rbank,
        raddress => raddress,
        validout => validout
    );

    memUP : MemDPLL
    GENERIC MAP(LAYER => LAYER)
    PORT MAP(
        clk => clk, reset => reset,

        rmem => rmem,
        rmemodd => rmemodd,
        rbank => rbank,
        raddress => raddress,
        Dout => dataout,

        Din => outCVDP,
        we => we,
        wmemodd => wmemodd,
        wBank => wBank,
        waddress => waddress
        );

END ARCHITECTURE rtl;