LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;

LIBRARY work;
USE work.YOLO_pkg.ALL;

--Aquí se comprueba el funcionamiento de una sóla capa

ENTITY layer9 IS
    PORT (
        clk : IN STD_LOGIC;
        reset : IN STD_LOGIC;
        start : IN STD_LOGIC;

        dataIN : IN STD_LOGIC_VECTOR(5 DOWNTO 0);
        validIN : STD_LOGIC;

        dataOUT : OUT STD_LOGIC_VECTOR((16*kernels(9))-1 DOWNTO 0);
        validOut : OUT STD_LOGIC
    );
END layer9;

ARCHITECTURE rtl OF layer9 IS

    CONSTANT rst_val : STD_LOGIC := '0';
    constant layer: INTEGER:=9;

    COMPONENT L9_BNROM
        PORT (            clk : IN STD_LOGIC;
            coefs : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
            address : IN unsigned(BNbitsAddress(layer) DOWNTO 0));
    END COMPONENT;

    COMPONENT L9_WROM
        PORT (            clk : IN STD_LOGIC;
            weight : OUT STD_LOGIC_VECTOR((8*kernels(layer))-1 DOWNTO 0);
            address : IN unsigned(weightsbitsAddress(layer) - 1 DOWNTO 0));
    END COMPONENT;

    COMPONENT ConvControl
        GENERIC (
            layer : INTEGER := 9
        );
        PORT (
            clk : IN STD_LOGIC;
            reset : IN STD_LOGIC;

            validIn : IN STD_LOGIC;

            startLBuffer : OUT STD_LOGIC;
            enableLBuffer : OUT STD_LOGIC;

            addressweight : OUT unsigned(weightsbitsAddress(LAYER) - 1 DOWNTO 0);
            addressbn : OUT unsigned(bits(filters(layer)/kernels(layer) - 1) - 1 DOWNTO 0);

            validOut : OUT STD_LOGIC
        );
    END COMPONENT;

    COMPONENT ConvDP9
        GENERIC (layer : INTEGER :=9);
        PORT (
            clk : IN STD_LOGIC;
            reset : IN STD_LOGIC;

            datain : IN STD_LOGIC_VECTOR(5 DOWNTO 0);
            Weights : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
            BIAS : IN STD_LOGIC_VECTOR(15 DOWNTO 0);

            startLbuffer : IN STD_LOGIC;
            enableLbuffer : IN STD_LOGIC;

            dataout : OUT STD_LOGIC_VECTOR(15 DOWNTO 0));
    END COMPONENT;

    SIGNAL startLBuffer : STD_LOGIC;
    SIGNAL enableLBuffer : STD_LOGIC;
    SIGNAL addressweight : unsigned(weightsbitsAddress(layer) - 1 DOWNTO 0);
    SIGNAL addressbn : unsigned(bits(filters(layer)/kernels(layer) - 1) - 1 DOWNTO 0);

    SIGNAL ROMweight : STD_LOGIC_VECTOR((8 * kernels(layer)) - 1 DOWNTO 0);
    SIGNAL ROMbias : STD_LOGIC_VECTOR(31 DOWNTO 0);
BEGIN

    --CONTROL
    ConvUC : ConvControl
    GENERIC MAP(Layer => Layer)
    PORT MAP(
        clk => clk, reset => reset,
        validIn => validIN,
        startLBuffer => startLBuffer, enableLBuffer => enableLBuffer,
        addressweight => addressweight,
        addressbn => addressbn,
        validOut => validOut);

    BNROM : L9_BNROM
    PORT MAP(clk => clk,
        coefs => ROMbias,
        address => addressbn
    );

    WROM : L9_WROM
    PORT MAP(clk => clk,
        weight => ROMweight,
        address => addressweight);

    kernelsDP : FOR I IN 0 TO kernels(layer) - 1 GENERATE
        ConvUP : ConvDP9
        GENERIC MAP(Layer => Layer)
        PORT MAP(
            clk => clk, reset => reset,
            DataIn => datain,
            weights => ROMweight((I * 8) + 7 DOWNTO I * 8),
            Bias => ROMbias((I * 16) + 15 DOWNTO I * 16),
            startLBuffer => startLBuffer,
            enableLBuffer => enableLBuffer,
            dataout => dataout((I * 16) + 15 DOWNTO I * 16)
        );
    END GENERATE kernelsDP;

END ARCHITECTURE rtl;