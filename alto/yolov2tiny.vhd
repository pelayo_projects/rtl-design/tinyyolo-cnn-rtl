LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;

LIBRARY work;
USE work.YOLO_pkg.ALL;

ENTITY yolov2tiny IS

    PORT (
        clk : IN STD_LOGIC;
        reset : IN STD_LOGIC;
        start : IN STD_LOGIC;

        In0 : IN STD_LOGIC_VECTOR(8 DOWNTO 0);
        In1 : IN STD_LOGIC_VECTOR(8 DOWNTO 0);
        In2 : IN STD_LOGIC_VECTOR(8 DOWNTO 0);
        In3 : IN STD_LOGIC_VECTOR(8 DOWNTO 0);
        In4 : IN STD_LOGIC_VECTOR(8 DOWNTO 0);
        In5 : IN STD_LOGIC_VECTOR(8 DOWNTO 0);
        In6 : IN STD_LOGIC_VECTOR(8 DOWNTO 0);
        In7 : IN STD_LOGIC_VECTOR(8 DOWNTO 0);
        In8 : IN STD_LOGIC_VECTOR(8 DOWNTO 0);
        validin : STD_LOGIC;
        
--        cvvalidOut : OUT STD_LOGIC;
--        cvdataout:  OUT STD_LOGIC_VECTOR(5 DOWNTO 0);

        dataout : OUT STD_LOGIC_VECTOR(31 DOWNTO 0); --salida DEBE SER de 32
        validout : OUT STD_LOGIC
    );

END ENTITY yolov2tiny;

ARCHITECTURE rtl OF yolov2tiny IS

    COMPONENT layer1
        PORT (
            clk : IN STD_LOGIC;
            reset : IN STD_LOGIC;
            start : IN STD_LOGIC;
            IN0 : IN STD_LOGIC_VECTOR(8 DOWNTO 0);
            IN1 : IN STD_LOGIC_VECTOR(8 DOWNTO 0);
            IN2 : IN STD_LOGIC_VECTOR(8 DOWNTO 0);
            IN3 : IN STD_LOGIC_VECTOR(8 DOWNTO 0);
            IN4 : IN STD_LOGIC_VECTOR(8 DOWNTO 0);
            IN5 : IN STD_LOGIC_VECTOR(8 DOWNTO 0);
            IN6 : IN STD_LOGIC_VECTOR(8 DOWNTO 0);
            IN7 : IN STD_LOGIC_VECTOR(8 DOWNTO 0);
            IN8 : IN STD_LOGIC_VECTOR(8 DOWNTO 0);
            validIN : STD_LOGIC;
            cvvalidOut : OUT STD_LOGIC;
            cvdataout:  OUT STD_LOGIC_VECTOR(5 DOWNTO 0);
            dataOUT : OUT STD_LOGIC_VECTOR(53 DOWNTO 0);
            validOut : OUT STD_LOGIC);
    END COMPONENT;

    COMPONENT layer2
        PORT (
            clk : IN STD_LOGIC;
            reset : IN STD_LOGIC;
            start : IN STD_LOGIC;
            dataIN : IN STD_LOGIC_VECTOR(53 DOWNTO 0);
            validIN : STD_LOGIC;
            cvvalidOut : OUT STD_LOGIC;
            cvdataout:  OUT STD_LOGIC_VECTOR(5 DOWNTO 0);
            dataOUT : OUT STD_LOGIC_VECTOR(53 DOWNTO 0);
            validOut : OUT STD_LOGIC
        );
    END COMPONENT;
    COMPONENT layer3
        PORT (
            clk : IN STD_LOGIC;
            reset : IN STD_LOGIC;
            start : IN STD_LOGIC;
            dataIN : IN STD_LOGIC_VECTOR(53 DOWNTO 0);
            validIN : STD_LOGIC;
            dataOUT : OUT STD_LOGIC_VECTOR(53 DOWNTO 0);
            validOut : OUT STD_LOGIC
        );
    END COMPONENT;

    COMPONENT layer4
        PORT (
            clk : IN STD_LOGIC;
            reset : IN STD_LOGIC;
            start : IN STD_LOGIC;
            dataIN : IN STD_LOGIC_VECTOR(53 DOWNTO 0);
            validIN : STD_LOGIC;
            dataOUT : OUT STD_LOGIC_VECTOR(53 DOWNTO 0);
            validOut : OUT STD_LOGIC
        );
    END COMPONENT;

    COMPONENT layer5
        PORT (
            clk : IN STD_LOGIC;
            reset : IN STD_LOGIC;
            start : IN STD_LOGIC;
            dataIN : IN STD_LOGIC_VECTOR(53 DOWNTO 0);
            validIN : STD_LOGIC;
            dataOUT : OUT STD_LOGIC_VECTOR(53 DOWNTO 0);
            validOut : OUT STD_LOGIC
        );
    END COMPONENT;

    COMPONENT layer6
        PORT (
            clk : IN STD_LOGIC;
            reset : IN STD_LOGIC;
            start : IN STD_LOGIC;
            dataIN : IN STD_LOGIC_VECTOR(53 DOWNTO 0);
            validIN : STD_LOGIC;
            dataOUT : OUT STD_LOGIC_VECTOR(53 DOWNTO 0);
            validOut : OUT STD_LOGIC
        );
    END COMPONENT;

    COMPONENT layer7
        PORT (
            clk : IN STD_LOGIC;
            reset : IN STD_LOGIC;
            start : IN STD_LOGIC;
            dataIN : IN STD_LOGIC_VECTOR(53 DOWNTO 0);
            validIN : STD_LOGIC;
            dataOUT : OUT STD_LOGIC_VECTOR(53 DOWNTO 0);
            validOut : OUT STD_LOGIC
        );
    END COMPONENT;

    COMPONENT layer8
        PORT (
            clk : IN STD_LOGIC;
            reset : IN STD_LOGIC;
            start : IN STD_LOGIC;
            dataIN : IN STD_LOGIC_VECTOR(53 DOWNTO 0);
            validIN : STD_LOGIC;
            dataOUT : OUT STD_LOGIC_VECTOR(5 DOWNTO 0);
            validOut : OUT STD_LOGIC
         
        );
    END COMPONENT;
    COMPONENT layer9
        PORT (
            clk : IN STD_LOGIC;
            reset : IN STD_LOGIC;
--            start : IN STD_LOGIC;
            dataIN : IN STD_LOGIC_VECTOR(5 DOWNTO 0);
            validIN : STD_LOGIC;
            dataOUT : OUT STD_LOGIC_VECTOR((16*kernels(9))-1 DOWNTO 0);
            validOut : OUT STD_LOGIC
        );
    END COMPONENT;

    signal dataoutL1: std_logic_vector(53 downto 0);
    signal validoutL1: std_logic;

    signal dataoutL2: std_logic_vector(53 downto 0);
    signal validoutL2: std_logic;

    signal dataoutL3: std_logic_vector(53 downto 0);
    signal validoutL3: std_logic;

    signal dataoutL4: std_logic_vector(53 downto 0);
    signal validoutL4: std_logic;

    signal dataoutL5: std_logic_vector(53 downto 0);
    signal validoutL5: std_logic;

    signal dataoutL6: std_logic_vector(53 downto 0);
    signal validoutL6: std_logic;

    signal dataoutL7: std_logic_vector(53 downto 0);
    signal validoutL7: std_logic;

    signal dataoutL8: std_logic_vector(5 downto 0);
    signal validoutL8: std_logic;

BEGIN

L1: layer1
PORT MAP(clk => clk, reset => reset, start => start,
IN0 => IN0, IN1 => IN1, IN2 => IN2, IN3 => IN3, IN4 => IN4,
IN5 => IN5, IN6 => IN6, IN7 => IN7, IN8 => IN8, validIn => validIn,
dataout =>dataoutL1,validOut => validoutL1);

L2: layer2
PORT MAP(clk => clk, reset => reset, start => start,
datain => dataoutL1, validIn => validoutL1,
dataout => dataoutL2, validout => validoutL2);

L3: layer3
PORT MAP(clk => clk, reset => reset, start => start,
datain => dataoutL2, validIn => validoutL2,
dataout => dataoutL3, validout => validoutL3);

L4: layer4
PORT MAP(clk => clk, reset => reset, start => start,
datain => dataoutL3, validIn => validoutL3,
dataout => dataoutL4, validout => validoutL4);

L5: layer5
PORT MAP(clk => clk, reset => reset, start => start,
datain => dataoutL4, validIn => validoutL4,
dataout => dataoutL5, validout => validoutL5);

L6: layer6
PORT MAP(clk => clk, reset => reset, start => start,
datain => dataoutL5, validIn => validoutL5,
dataout => dataoutL6, validout => validoutL6);

L7: layer7
PORT MAP(clk => clk, reset => reset, start => start,
datain => dataoutL6, validIn => validoutL6,
dataout => dataoutL7, validout => validoutL7);

L8: layer8
PORT MAP(clk => clk, reset => reset, start => start,
datain => dataoutL7, validIn => validoutL7,
dataout => dataoutL8, validout => validoutL8);

L9:layer9
PORT MAP(clk => clk, reset => reset, 
datain => dataoutL8, validIn => validoutL8,
dataout => dataout, validout => validout);

END ARCHITECTURE rtl;