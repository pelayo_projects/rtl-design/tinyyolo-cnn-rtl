LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;

LIBRARY work;
USE work.YOLO_pkg.ALL;

--Aquí se comprueba el funcionamiento de una sóla capa

ENTITY layer2 IS
    PORT (
        clk : IN STD_LOGIC;
        reset : IN STD_LOGIC;
        start : IN STD_LOGIC;

        dataIN : IN STD_LOGIC_VECTOR(53 DOWNTO 0);
        validIN : STD_LOGIC;
        
        cvvalidOut : OUT STD_LOGIC;
        cvdataout:  OUT STD_LOGIC_VECTOR(5 DOWNTO 0);

        dataOUT : OUT STD_LOGIC_VECTOR(53 DOWNTO 0);
        validOut : OUT STD_LOGIC
    );
END layer2;

ARCHITECTURE rtl OF layer2 IS

    CONSTANT rst_val : STD_LOGIC := '0';
    constant layer: INTEGER:=2;

    COMPONENT L2_BNROM
        PORT (            clk : IN STD_LOGIC;
            coefs : OUT STD_LOGIC_VECTOR((32*kernels(layer))-1 DOWNTO 0);
            address : IN unsigned(BNbitsAddress(layer) DOWNTO 0));
    END COMPONENT;

    COMPONENT L2_WROM
        PORT (            clk : IN STD_LOGIC;
            weight : OUT STD_LOGIC_VECTOR((9*kernels(layer))-1 DOWNTO 0);
            address : IN unsigned(weightsbitsAddress(layer) - 1 DOWNTO 0));
    END COMPONENT;

    COMPONENT ConvControl
        GENERIC (
            layer : INTEGER := 2
        );
        PORT (
            clk : IN STD_LOGIC;
            reset : IN STD_LOGIC;

            validIn : IN STD_LOGIC;

            startLBuffer : OUT STD_LOGIC;
            enableLBuffer : OUT STD_LOGIC;

            addressweight : OUT unsigned(weightsbitsAddress(LAYER) - 1 DOWNTO 0);
            addressbn : OUT unsigned(bits(filters(layer)/kernels(layer) - 1) - 1 DOWNTO 0);

            validOut : OUT STD_LOGIC
        );
    END COMPONENT;

    COMPONENT ConvDP
        GENERIC (layer : INTEGER := 2);
        PORT (
            clk : IN STD_LOGIC;
            reset : IN STD_LOGIC;

            datain : IN STD_LOGIC_VECTOR((9 * layerbits(layer)) - 1 DOWNTO 0);
            Weights : IN STD_LOGIC_VECTOR(8 DOWNTO 0);
            Ynorm : IN signed(15 DOWNTO 0);
            Bnorm : IN signed(15 DOWNTO 0);

            startLbuffer : IN STD_LOGIC;
            enableLbuffer : IN STD_LOGIC;

            dataout : OUT STD_LOGIC_VECTOR(5 DOWNTO 0));
    END COMPONENT;

    COMPONENT MaxPoolControl
        GENERIC (
            Layer : INTEGER := 2
        );
        PORT (
            clk : IN STD_LOGIC;
            reset : IN STD_LOGIC;
            validIn : IN STD_LOGIC;
            val_d1 : OUT STD_LOGIC;
            enLBuffer : OUT STD_LOGIC;
            validOut : OUT STD_LOGIC
        );
    END COMPONENT;
    COMPONENT MaxPoolDP
        GENERIC (Layer : INTEGER := 2);
        PORT (
            clk, reset : IN STD_LOGIC;
            val_d1 : IN STD_LOGIC;
            enLBuffer : IN STD_LOGIC;
            datain : IN STD_LOGIC_VECTOR(5 DOWNTO 0);
            dataout : OUT STD_LOGIC_VECTOR(5 DOWNTO 0)
        );
    END COMPONENT;

    COMPONENT MemControl
        GENERIC (
            layer : INTEGER := 2);
        PORT (
            clk : IN STD_LOGIC;
            reset : IN STD_LOGIC;
            start : IN STD_LOGIC;
            we : IN STD_LOGIC;
            rMem : OUT unsigned(bits(kernels(layer)) - 1 DOWNTO 0);
            rMemOdd : OUT STD_LOGIC;
            address0 : OUT unsigned(bitsAddress(layer) - 1 DOWNTO 0);
            address1 : OUT unsigned(bitsAddress(layer) - 1 DOWNTO 0);
            address2 : OUT unsigned(bitsAddress(layer) - 1 DOWNTO 0);
            padding : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
            kernelCol : OUT unsigned(1 DOWNTO 0);
            kernelRow : OUT unsigned(1 DOWNTO 0);
            enableKernel : OUT STD_LOGIC;
            validOut : OUT STD_LOGIC;
            weRAM : OUT STD_LOGIC;
            wMemOdd : OUT STD_LOGIC;
            wBank : OUT unsigned(3 DOWNTO 0);
            waddress : OUT unsigned(bitsAddress(layer) - 1 DOWNTO 0));
    END COMPONENT;

    COMPONENT MemDP
        GENERIC (layer : INTEGER := 2);
        PORT (
            clk : IN STD_LOGIC;
            reset : IN STD_LOGIC;
            Din : IN STD_LOGIC_VECTOR((kernels(layer) * 6) - 1 DOWNTO 0);
            rMem : IN unsigned(bits(kernels(layer)) - 1 DOWNTO 0);
            rMemOdd : IN STD_LOGIC;
            address0 : IN unsigned(bitsAddress(layer) - 1 DOWNTO 0);
            address1 : IN unsigned(bitsAddress(layer) - 1 DOWNTO 0);
            address2 : IN unsigned(bitsAddress(layer) - 1 DOWNTO 0);
            enableKernel : IN STD_LOGIC;
            padding : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
            kernelCol : IN unsigned(1 DOWNTO 0);
            kernelRow : IN unsigned(1 DOWNTO 0);
            we : IN STD_LOGIC;
            wMemOdd : IN STD_LOGIC;
            wBank : IN unsigned(3 DOWNTO 0);
            waddress : IN unsigned(bitsAddress(layer) - 1 DOWNTO 0);
            Dout : OUT STD_LOGIC_VECTOR((9 * 6) - 1 DOWNTO 0));
    END COMPONENT;

    SIGNAL startLBuffer : STD_LOGIC;
    SIGNAL enableLBuffer : STD_LOGIC;
    SIGNAL addressweight : unsigned(weightsbitsAddress(layer) - 1 DOWNTO 0);
    SIGNAL addressbn : unsigned(bits(filters(layer)/kernels(layer) - 1) - 1 DOWNTO 0);
    SIGNAL validoutCV : STD_LOGIC;

    SIGNAL ROMweight : STD_LOGIC_VECTOR((9 * kernels(layer)) - 1 DOWNTO 0);
    SIGNAL invROMweight : STD_LOGIC_VECTOR((9 * kernels(layer)) - 1 DOWNTO 0);

    SIGNAL ROMcoefs : STD_LOGIC_VECTOR((32 * kernels(layer)) - 1 DOWNTO 0);

    SIGNAL outCVDP : STD_LOGIC_VECTOR((6 * kernels(layer)) - 1 DOWNTO 0);

    SIGNAL valid_d1 : STD_LOGIC;
    SIGNAL enLbuffer : STD_LOGIC;
    SIGNAL validoutMP : STD_LOGIC;

    SIGNAL outMPDP : STD_LOGIC_VECTOR((6 * kernels(layer)) - 1 DOWNTO 0);

    SIGNAL weRAM : STD_LOGIC;
    SIGNAL wMemOdd : STD_LOGIC;
    SIGNAL wBank : unsigned(3 DOWNTO 0);
    SIGNAL waddress : unsigned(bitsAddress(layer) - 1 DOWNTO 0);

    SIGNAL rMem : unsigned(bits(kernels(layer)) - 1 DOWNTO 0);
    SIGNAL rMemOdd : STD_LOGIC;
    SIGNAL address0 : unsigned(bitsAddress(layer) - 1 DOWNTO 0);
    SIGNAL address1 : unsigned(bitsAddress(layer) - 1 DOWNTO 0);
    SIGNAL address2 : unsigned(bitsAddress(layer) - 1 DOWNTO 0);
    SIGNAL padding : STD_LOGIC_VECTOR(2 DOWNTO 0);
    SIGNAL kernelCol : unsigned(1 DOWNTO 0);
    SIGNAL kernelRow : unsigned(1 DOWNTO 0);
    SIGNAL enableKernel : STD_LOGIC;
    SIGNAL memOE : STD_LOGIC;

    SIGNAL outMem : STD_LOGIC_VECTOR(53 DOWNTO 0);

BEGIN

    --CONTROL

    ConvUC : ConvControl
    GENERIC MAP(Layer => Layer)
    PORT MAP(
        clk => clk, reset => reset,
        validIn => validIN,
        startLBuffer => startLBuffer, enableLBuffer => enableLBuffer,
        addressweight => addressweight,
        addressbn => addressbn,
        validOut => validoutCV);

    MPUC : MaxPoolControl
    GENERIC MAP(Layer => layer)
    PORT MAP(
        clk => clk,
        reset => reset,
        validIn => validoutCV,
        val_d1 => valid_d1, enLBuffer => enLbuffer,
        validOut => validoutMP
    );

    BNROM : L2_BNROM
    PORT MAP(clk => clk,
        coefs => ROMcoefs,
        address => addressbn
    );

    WROM : L2_WROM
    PORT MAP(clk => clk,
        weight => ROMweight,
        address => addressweight);

    invweight : PROCESS (ROMweight)
    BEGIN
        FOR i IN 0 TO 8 LOOP
            FOR j IN 0 TO kernels(layer) - 1 LOOP
                invROMweight((j*9)+i) <= ROMweight(((j+1)*9)-1 - i);
            END LOOP;
        END LOOP;
    END PROCESS invweight;

    kernelsDP : FOR I IN 0 TO kernels(layer) - 1 GENERATE
        ConvUP : ConvDP
        GENERIC MAP(Layer => Layer)
        PORT MAP(
            clk => clk, reset => reset,
            DataIn => datain,
            weights => invROMweight((I * 9) + 8 DOWNTO I * 9),
            Ynorm => signed(ROMcoefs((I * 32) + 15 DOWNTO I * 32)),
            Bnorm => signed(ROMcoefs((I * 32) + 31 DOWNTO (I * 32) + 16)),
            startLBuffer => startLBuffer,
            enableLBuffer => enableLBuffer,
            dataout => outCVDP((I * 6) + 5 DOWNTO I * 6)
        );

        MPDP : MaxPoolDP
        GENERIC MAP(Layer => layer)
        PORT MAP(
            clk => clk, reset => reset,
            val_d1 => valid_d1, enLbuffer => enLbuffer,
            datain => outCVDP((I * 6) + 5 DOWNTO I * 6),
            dataout => outMPDP((I * 6) + 5 DOWNTO I * 6)
        );

    END GENERATE kernelsDP;

    -- MEMORIA

    memUC : MemControl
    GENERIC MAP(LAYER => LAYER)
    PORT MAP(
        clk => clk, reset => reset,
        start => start, we => validoutMP,
        rmem => rmem,
        rmemodd => rmemodd,
        address0 => address0,
        address1 => address1,
        address2 => address2,
        padding => padding,
        kernelCol => kernelCol,
        kernelrow => kernelrow,
        enableKernel => enableKernel,
        validout => validout,
        weram => weram,
        wmemodd => wmemodd,
        wBank => wBank,
        waddress => waddress
    );

    memUP : MemDP
    GENERIC MAP(LAYER => LAYER)
    PORT MAP(
        clk => clk, reset => reset,

        Din => outMPDP,

        rmem => rmem,
        rmemodd => rmemodd,
        address0 => address0,
        address1 => address1,
        address2 => address2,

        enableKernel => enableKernel,
        padding => padding,
        kernelCol => kernelCol,
        kernelrow => kernelrow,

        we => weram,
        wmemodd => wmemodd,
        wBank => wBank,
        waddress => waddress,

        Dout => outMem
    );
    
dataout<=outmem;
cvvalidout <= validoutcv;
cvdataout <= outcvDP(5 downto 0);
END ARCHITECTURE rtl;